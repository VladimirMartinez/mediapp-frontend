import { Component, OnInit } from '@angular/core';
import * as decode from 'jwt-decode';
import { TOKEN_NAME } from '../../_shared/var.constant';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  nombre: string;
  roles= [];
  constructor() { }

  ngOnInit() {
    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        const decodedToken = decode(tk.access_token);
        
        this.nombre=decodedToken.user_name;
        this.roles=decodedToken.authorities;
        console.log(this.roles);

        

        
  }

}
