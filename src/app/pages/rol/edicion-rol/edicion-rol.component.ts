import { Component, OnInit } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { RolService } from '../../../_service/rol.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-edicion-rol',
  templateUrl: './edicion-rol.component.html',
  styleUrls: ['./edicion-rol.component.css']
})
export class EdicionRolComponent implements OnInit {
  rol: Rol;
  id: number;
  nombre: string;
  descripcion: string;
  edicion: boolean = false;

  constructor(private rolService: RolService, private route:ActivatedRoute, private router: Router, public snackBar: MatSnackBar) { }


  ngOnInit() {
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id'] != null;
      this.initForm();
    })
  }

  private initForm(){
    if(this.edicion)
    {
      this.rolService.listarPorId(this.id).subscribe(data=>{
        this.id=data.idRol;
        this.nombre=data.nombre;
        this.descripcion=data.descripcion;
      })
    }
  }

  operar(){
    this.rol = new Rol();

    this.rol.idRol=this.id;
    this.rol.nombre=this.nombre;
    this.rol.descripcion=this.descripcion;
    console.log(this.rol);
    if(this.edicion){
      this.rolService.modificar(this.rol).subscribe(data=>{
        this.rolService.listar().subscribe(rol=>{
          this.rolService.rolCambio.next(rol);
          this.rolService.mensaje.next('Se modifico');
        })
        
      });
    }else{
      this.rolService.registrar(this.rol).subscribe(data=>{
        this.rolService.listar().subscribe(rol=>{
          this.rolService.rolCambio.next(rol);
          this.rolService.mensaje.next('Se registro');
        })
      })
    }
    this.router.navigate(['rol'])
  }

}
