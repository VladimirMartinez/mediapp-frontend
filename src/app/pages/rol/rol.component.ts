import { Component, OnInit, ViewChild } from '@angular/core';
import { Rol } from '../../_model/rol';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { RolService } from '../../_service/rol.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  lista: Rol[]=[];
  displayedColumns=['idRol', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<Rol>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private serviceRol: RolService, private snackBar: MatSnackBar, public route: ActivatedRoute) { }

  ngOnInit() {
    this.serviceRol.rolCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.serviceRol.mensaje.subscribe(data=>{
        this.snackBar.open(data, 'Aviso', {duration: 2000});
      })
    })
    this.serviceRol.listaRolPageable(0, 10).subscribe(data=>{
      let rol = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(rol);
      this.dataSource.sort= this.sort;
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  mostrarMas(e: any){
    console.log(e);
    this.serviceRol.listaRolPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let rol = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(rol);
      
      this.dataSource.sort = this.sort;
      
    });
  }


}
