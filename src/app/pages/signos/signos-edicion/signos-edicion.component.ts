import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../../_model/paciente';
import { Signos } from '../../../_model/signos';
import { PacienteService } from '../../../_service/paciente.service';
import { SignosService } from '../../../_service/signos.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Observable, empty } from 'rxjs';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DialogoSignosComponent } from '../dialogo-signos/dialogo-signos.component';
import { Conditional } from '@angular/compiler';


@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {
  filteredOptions: Observable<any[]>;
  filteredOptionsMedico: Observable<any[]>;
  pacientes: Paciente[] = [];
  signos: Signos;
  temperatura:string;
  pulso: string;
  ritmoRespiratorio: string;
  id:number;



 
  
  myControlSignos: FormControl = new FormControl();
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  pacienteSeleccionado: Paciente;
  mensaje: string;
  form: FormGroup;
  
  
  edicion: boolean=false;
  
  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, builder: FormBuilder, private pacienteService: PacienteService, private signosService: SignosService, public snackBar: MatSnackBar) { 
    this.form=builder.group({
      'paciente':this.myControlSignos
    })
  }

  ngOnInit() {
  
  this.listarPacientes();

   

    this.filteredOptions = this.myControlSignos.valueChanges
      .pipe(
        startWith(null),
        map(val => this.filter(val))
      );

      this.route.params.subscribe((params: Params) => {
        this.id = params['id'];
        this.edicion = params['id'] != null;
        this.initForm();
   
      });

    
  }
 
  private initForm(){
    if(this.edicion){
      this.signosService.listarPacientePorId(this.id).subscribe(data=>{
        this.id= data.idSigno;
        let paciente=data.paciente;
        this.temperatura =data.temperatura;
        this.pulso=data.pulso;
        this.ritmoRespiratorio=data.ritmoRespiratorio;
        
        this.form= new FormGroup({
          'paciente': new FormControl(paciente)
         
        })
 
     
      })
    }
    
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }
  listarPacientes() {
    
    this.pacienteService.listarPacientes().subscribe(data=>{
      this.pacienteService.pacienteCambio.next(data);
    })

      this.pacienteService.pacienteCambio.subscribe(data => {
        this.pacientes = data;
    })

  
}

disableControl( condition : boolean ) {
  const action = condition ? 'disable' : 'enable';
  this[action]();
}
  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }
  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }
  operar() {
    if(this.pulso===undefined || this.temperatura==undefined || this.ritmoRespiratorio==undefined ){
      this.snackBar.open("Llenar Todos los Campos", "Aviso", { duration: 2000 })
        
    }else{
   
    this.signos = new Signos();
    
    this.signos.paciente = this.form.value['paciente'];
    this.signos.idSigno=this.id;
    console.log(this.id);
    this.signos.fecha = this.fechaSeleccionada;
    this.signos.pulso=this.pulso;
    this.signos.temperatura=this.temperatura;
    this.signos.ritmoRespiratorio=this.ritmoRespiratorio;
    console.log(this.signos);
    
    if(this.edicion){
      this.signosService.modificar(this.signos).subscribe(data=>{
        this.signosService.listarSignos().subscribe(signos=>{
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next('Se modificó');
        });
      });
    }else{
      this.signosService.registrar(this.signos).subscribe(data=>{
        this.signosService.listarSignos().subscribe(signos=>{
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next('Se registró');
        })
        
      })
    }
    this.router.navigate(['signos'])

  }
  }
 
  openDialog(paciente: Paciente): void{
    let pac = paciente !=null? paciente: new Paciente();
    let dialogRef = this.dialog.open(DialogoSignosComponent,{
      width: '400px',
      disableClose:true,

      data:pac
    })
  }

  

}
