import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '../../../../../node_modules/@angular/material';
import { Paciente } from '../../../_model/paciente';
import { PacienteService } from '../../../_service/paciente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialogo-signos',
  templateUrl: './dialogo-signos.component.html',
  styleUrls: ['./dialogo-signos.component.css']
})
export class DialogoSignosComponent implements OnInit {

  paciente: Paciente;

  idPaciente:number;
  nombres: string;
  apellidos: string;
  dni: string;
  telefono:string;
  direccion: string;
  email: string;

  constructor(private zone:NgZone, private snackBar: MatSnackBar, private router: Router, public dialogRef: MatDialogRef<DialogoSignosComponent>,
  @Inject(MAT_DIALOG_DATA)public data: Paciente, private pacienteService: PacienteService) { }


  ngOnInit() {
    this.paciente = new Paciente();
    this.paciente.idPaciente = this.data.idPaciente;
    this.paciente.nombres= this.data.nombres;
    this.paciente.apellidos= this.data.apellidos;
    this.paciente.dni= this.data.dni;
    this.paciente.telefono=this.data.telefono;
    this.paciente.direccion=this.data.direccion;
    this.paciente.email= this.data.email;
  }
  operar(){
    this.paciente.nombres=this.nombres;
    this.paciente.apellidos=this.apellidos;
    this.paciente.telefono=this.telefono;
    this.paciente.dni=this.dni;
    this.paciente.direccion=this.direccion;
    this.paciente.email=this.email;
    if(this.paciente.nombres==undefined || this.paciente.apellidos==undefined|| this.paciente.dni==undefined|| this.paciente.direccion==undefined || this.paciente.telefono==undefined){
      this.snackBar.open("Debe de llenar todos los campos *","Aviso", {duration:2000});
      return;
    }else{
      console.log(this.paciente);
     this.pacienteService.registrar(this.paciente).subscribe(data=>{
        this.pacienteService.listarPacientes().subscribe(paciente=>{
          this.pacienteService.pacienteCambio.next(paciente);
        })
        

      });
      
      
      this.dialogRef.close();
    
     
      this.snackBar.open("Se registro exitosamente","Aviso", {duration:2000});
    }
   
    
  }
  cancelar() {
    this.dialogRef.close();
  }

}
