import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Signos } from '../../../_model/signos';
import { SignosService } from '../../../_service/signos.service';

@Component({
  selector: 'app-modal-eliminar',
  templateUrl: './modal-eliminar.component.html',
  styleUrls: ['./modal-eliminar.component.css']
})
export class ModalEliminarComponent implements OnInit {

  signos : Signos;
  nombres: string;
  apellidos: string;
  idSigno: number;
  constructor(public dialogRef: MatDialogRef<ModalEliminarComponent>,
  @Inject(MAT_DIALOG_DATA) public data: Signos,
  private serviceSignos: SignosService) { }

  ngOnInit() {
    this.signos = new Signos();
    this.signos.idSigno= this.data.idSigno;
    this.idSigno= this.data.idSigno;
    this.nombres=this.data.paciente.nombres;
    this.apellidos=this.data.paciente.apellidos;
    console.log(this.nombres=this.data.paciente.nombres);
  }
  operar(){
    console.log(this.idSigno);
   this.serviceSignos.eliminar(this.idSigno).subscribe(data=>{
    this.serviceSignos.listarSignos().subscribe(signos=>{
      this.serviceSignos.signosCambio.next(signos);
    })
    
   });
    this.cancelar();
  }

  cancelar(){
    this.dialogRef.close();
  }

}
