import { Component, OnInit, ViewChild } from '@angular/core';
import { Signos } from '../../_model/signos';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { SignosService } from '../../_service/signos.service';
import { ActivatedRoute } from '@angular/router';
import { ModalEliminarComponent } from './modal-eliminar/modal-eliminar.component';


@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {
  
  lista: Signos[]=[];
  displayedColumns=[ 'paciente','fecha', 'acciones'];
  dataSource: MatTableDataSource<Signos>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  

  constructor(public dialog: MatDialog, private signosService: SignosService, private snackBar: MatSnackBar, public route: ActivatedRoute)  { }

  ngOnInit() {
    this.signosService.signosCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.signosService.mensaje.subscribe(data=>{
        this.snackBar.open(data, 'Aviso', {duration:2000});
      });
    });

    this.signosService.listarSignosPageable(0, 10).subscribe(data=>{
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(signos);
      this.dataSource.sort = this.sort;
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  mostrarMas(e: any){
    console.log(e);
    this.signosService.listarSignosPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(signos);
      
      this.dataSource.sort = this.sort;
      
    });

   
  }
  openDialogoEliminar(signos: Signos): void{
    let sig = signos !=null? signos: new Signos();
    let dialogRef = this.dialog.open(ModalEliminarComponent,{
      width: '400px',
      disableClose: true,

      data:sig
    })
  }

}
