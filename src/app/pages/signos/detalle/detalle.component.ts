import { Component, OnInit } from '@angular/core';
import { SignosService } from '../../../_service/signos.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Signos } from '../../../_model/signos';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  signos: Signos;
  
  fecha: Date;
  pulso: string;
  temperatura: string;
  ritmoRespiratorio: string;
  nombres: string;
  apellidos: string;
  id: number

  constructor(private signosService: SignosService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.params.subscribe((params:Params)=>{
      this.id=params['id'];
      this.form();
      
    })

  }
 private form(){
  console.log(this.id);
   this.signosService.listarPacientePorId(this.id).subscribe(data=>{
    this.nombres=data.paciente.nombres;
    this.apellidos=data.paciente.apellidos;
    this.fecha=data.fecha;
    this.pulso=data.pulso;
    this.temperatura=data.temperatura;
    this.ritmoRespiratorio=data.ritmoRespiratorio;
   })

  }

}
