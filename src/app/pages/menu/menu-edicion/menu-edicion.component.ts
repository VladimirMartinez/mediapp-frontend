import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../_model/menu';
import { MenuService } from '../../../_service/menu.service';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  menu: Menu;
  id: number;
  icono: string;
  nombre: string;
  url: string;
  edicion: boolean= false;
 

  constructor(private menuService: MenuService, private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params )=>{
      this.id=params['id'];
      this.edicion=params['id'] != null;
      this.initForm();
    });
  }

  
  private initForm(){
    if(this.edicion){
      this.menuService.listarPorId(this.id).subscribe(data=>{
        this.id= data.idMenu;
        this.icono= data.icono;
        this.nombre=data.nombre;
        this.url=data.url;
      })
    }
  }

  operar(){
    this.menu = new Menu();

    this.menu.idMenu = this.id;
    this.menu.icono=this.icono;
    this.menu.nombre=this.nombre;
    this.menu.url=this.url;
    console.log(this.menu);
    if(this.edicion){
      this.menuService.modificar(this.menu).subscribe(data=>{
        this.menuService.listar().subscribe(menu=>{
          this.menuService.menuCambio.next(menu);
          this.menuService.mensaje.next('Se modifico');
        })
      });
    }else{
      this.menuService.registrar(this.menu).subscribe(data=>{
        this.menuService.listar().subscribe(menu=>{
          this.menuService.menuCambio.next(menu);
          this.menuService.mensaje.next('Se registro');
        })
      })
    }
    this.router.navigate(['menu'])
    
  }
}
