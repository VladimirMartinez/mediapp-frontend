import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../_model/usuario';
import { Rol } from '../../../_model/rol';
import { UsuarioService } from '../../../_service/usuario.service';
import { RolService } from '../../../_service/rol.service';
import { MatSnackBar } from '@angular/material';
import { Params, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-usuario-rol-edicion',
  templateUrl: './usuario-rol-edicion.component.html',
  styleUrls: ['./usuario-rol-edicion.component.css']
})
export class UsuarioRolEdicionComponent implements OnInit {

  id: number;
  usuario: Usuario;
  edicion: boolean = false;

  username: string;
  password: string;
  enabled: boolean;
  roles: Rol[] = []
  rolesSeleccionados: Rol[]= [];
  rolesSeleccionado: Rol;
  mensaje: string;
  rolesGuardados: Rol[]=[];
  rol: any;


  constructor(private usuarioService: UsuarioService, private rolService: RolService, private route: ActivatedRoute, private router: Router,
              public snackBar: MatSnackBar) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
    this.listarRoles();

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
     
    });

    
  }

  private initForm() {
    if (this.edicion) {
      this.usuarioService.listarUsuarioPorId(this.id).subscribe(data => {
        this.id = data.idUsuario;
        this.username = data.username;
        this.password = data.password;
        this.enabled = data.enabled;
        this.rolesGuardados = data.roles;

        console.log(this.password);
      });
    }
  }

  aceptar() {
    let usuario = new Usuario();
    usuario.idUsuario = this.id;
    usuario.username = this.username;
    usuario.enabled = this.enabled;
    usuario.password = this.password;
  
    usuario.roles = this.rolesGuardados.concat(this.rolesSeleccionados);
     
    
    console.log(usuario);
    this.usuarioService.modificar(usuario).subscribe(data => {
      this.usuarioService.listarUsuarios().subscribe(dato => {
        this.usuarioService.usuarioCambio.next(dato);
        this.usuarioService.mensaje.next('Se realizo la inserción');
        this.router.navigate(['usuariorol']);
      });
    });

  }

  listarRoles() {
    this.rolService.listar().subscribe(data => {
      this.roles = data;
    });
  }

  agregarRoles() {
    if (this.rolesSeleccionado) {
      let cont = 0;
      console.log(this.roles);
      for(let j = 0; j< this.rolesGuardados.length; j++){
        let rolGuardado = this.rolesGuardados[j];
        
        if(this.rolesSeleccionado.idRol==rolGuardado.idRol){
          cont++;
        break;
        }else{
        for (let i = 0; i < this.rolesSeleccionados.length; i++) {
          this.rol = this.rolesSeleccionados[i];
          if (this.rol.idRol === this.rolesSeleccionado.idRol) {
            cont++;
            break;
          }
         
        }
      }
      }        
      if (cont > 0) {
        this.mensaje = `El Rol ya se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        
        this.rolesSeleccionados.push(this.rolesSeleccionado);
        
      }
    } else {
      this.mensaje = `Debe agregar un examen`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  

  limpiarControles() {
    this.rolesSeleccionados = [];
    this.rolesGuardados = [];
  }


}
