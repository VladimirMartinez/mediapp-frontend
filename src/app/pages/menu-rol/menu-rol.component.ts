import { Component, OnInit, ViewChild } from '@angular/core';
import { Menu } from '../../_model/menu';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { MenuService } from '../../_service/menu.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu-rol',
  templateUrl: './menu-rol.component.html',
  styleUrls: ['./menu-rol.component.css']
})
export class MenuRolComponent implements OnInit {
  lista: Menu[]=[];
  displayedColumns=['idMenu', 'nombre', 'acciones' ];
  dataSource: MatTableDataSource<Menu>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(private serviceMenu: MenuService, private snackBar: MatSnackBar, public route: ActivatedRoute) { }

  ngOnInit() {

    this.serviceMenu.menuCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.serviceMenu.mensaje.subscribe(data=>{
        this.snackBar.open(data, 'Aviso', {duration: 2000});
      })
    })
    this.serviceMenu.listaMenuPageable(0, 10).subscribe(data=>{
      let menu = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menu);
      this.dataSource.sort= this.sort;

    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  mostrarMas(e: any){
    console.log(e);
    this.serviceMenu.listaMenuPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let menu = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(menu);
      
      this.dataSource.sort = this.sort;
      
    });
  }

}
