import { Component, OnInit } from '@angular/core';
import { Rol } from '../../../_model/rol';
import { MatSnackBar } from '@angular/material';
import { RolService } from '../../../_service/rol.service';
import { MenuService } from '../../../_service/menu.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Menu } from '../../../_model/menu';

@Component({
  selector: 'app-menu-rol-edicion',
  templateUrl: './menu-rol-edicion.component.html',
  styleUrls: ['./menu-rol-edicion.component.css']
})
export class MenuRolEdicionComponent implements OnInit {

  roles: Rol[] = []
  rolesSeleccionados: Rol[]= [];
  rolesSeleccionado: Rol;
  mensaje: string;
  id: number;
  edicion: boolean=false;
  nombre:string;
  icono:string;
  url:string;

  rolesGuardados: Rol[]=[];
  rol: any;
  menu: Menu;
  
  constructor(private menuService: MenuService,public snackBar: MatSnackBar, private rolService: RolService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.listarRoles();
    this.route.params.subscribe((params: Params)=>{
      this.id= params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    })
  }
  private initForm(){
    if(this.edicion){
      this.menuService.listarPorId(this.id).subscribe(data=>{
        this.id=data.idMenu;
        this.nombre = data.nombre;
        this.icono = data.icono;
        this.url=data.url;
       
     
        this.rolesGuardados = data.roles;
        console.log(this.id, this.nombre, this.roles);
      })
    }
  }

  agregarRoles() {
    if (this.rolesSeleccionado) {
      let cont = 0;
      console.log(this.roles);
      for(let j = 0; j< this.rolesGuardados.length; j++){
        let rolGuardado = this.rolesGuardados[j];
        
        if(this.rolesSeleccionado.idRol==rolGuardado.idRol){
          cont++;
        break;
        }else{
        for (let i = 0; i < this.rolesSeleccionados.length; i++) {
          this.rol = this.rolesSeleccionados[i];
          if (this.rol.idRol === this.rolesSeleccionado.idRol) {
            cont++;
            break;
          }
         
        }
      }
      }        
      if (cont > 0) {
        this.mensaje = `El Rol ya se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        
        this.rolesSeleccionados.push(this.rolesSeleccionado);
        
      }
    } else {
      this.mensaje = `Debe agregar un rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }
  aceptar(){
    this.menu = new Menu();
    this.menu.idMenu = this.id;
    this.menu.nombre=this.nombre;
    this.menu.icono=this.icono;
    this.menu.url=this.url;
    
    this.menu.roles=this.rolesGuardados.concat(this.rolesSeleccionados);
    
    console.log(this.menu);
    this.menuService.modificar(this.menu).subscribe(data=>{
      this.menuService.listar().subscribe(dato=>{
        this.menuService.menuCambio.next(dato);
        this.menuService.mensaje.next('Se actualizo');
        this.router.navigate(['menurol']);
      })
      
    })
   
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  listarRoles(){
    this.rolService.listar().subscribe(data=>{
      this.roles=data;
    })
  }

}
