
import { Paciente } from "./paciente";
export class Signos{
     idSigno: number;
     fecha: Date;
     temperatura: any;
     pulso: string;
     ritmoRespiratorio: string;
     paciente: Paciente;

}