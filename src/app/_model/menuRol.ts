import { Menu } from "./menu";
import { Rol } from "./rol";

export class MenuRol{
    menu: Menu;
    rol: Rol;
}