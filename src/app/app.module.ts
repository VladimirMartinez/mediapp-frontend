import { ServerErrorsInterceptor } from './_shared/server-errors.interceptor';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { Not403Component } from './pages/not403/not403.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { DialogoComponent } from './pages/medico/dialogo/dialogo.component';
import { LoginComponent } from './login/login.component';
import { BuscarComponent } from './pages/consulta/buscar/buscar.component';
import { DialogoDetalleComponent } from './pages/consulta/buscar/dialogo-detalle/dialogo-detalle.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TokenComponent } from './login/recuperar/token/token.component';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { SignosComponent } from './pages/signos/signos.component';
import { SignosEdicionComponent } from './pages/signos/signos-edicion/signos-edicion.component';
import { DialogoSignosComponent } from './pages/signos/dialogo-signos/dialogo-signos.component';
import { ModalEliminarComponent } from './pages/signos/modal-eliminar/modal-eliminar.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { DetalleComponent } from './pages/signos/detalle/detalle.component';
import { MenuComponent } from './pages/menu/menu.component';
import { MenuEdicionComponent } from './pages/menu/menu-edicion/menu-edicion.component';
import { RolComponent } from './pages/rol/rol.component';
import { EdicionRolComponent } from './pages/rol/edicion-rol/edicion-rol.component';
import { MenuRolComponent } from './pages/menu-rol/menu-rol.component';
import { MenuRolEdicionComponent } from './pages/menu-rol/menu-rol-edicion/menu-rol-edicion.component';
import { UsuarioRolComponent } from './pages/usuario-rol/usuario-rol.component';
import { UsuarioRolEdicionComponent } from './pages/usuario-rol/usuario-rol-edicion/usuario-rol-edicion.component';


@NgModule({
  declarations: [
    AppComponent,
    PacienteComponent,
    PacienteEdicionComponent,
    ConsultaComponent,
    EspecialidadComponent,
    ExamenComponent,
    MedicoComponent,
    Not403Component,
    EspecialidadEdicionComponent,
    ExamenEdicionComponent,
    DialogoComponent,
    LoginComponent,
    BuscarComponent,
    DialogoDetalleComponent,
    ReporteComponent,
    TokenComponent,
    RecuperarComponent,
    EspecialComponent,
    SignosComponent,
    SignosEdicionComponent,
    DialogoSignosComponent,
    ModalEliminarComponent,
    PerfilComponent,
    DetalleComponent,
    MenuComponent,
    MenuEdicionComponent,
    RolComponent,
    EdicionRolComponent,
    MenuRolComponent,
    MenuRolEdicionComponent,
    UsuarioRolComponent,
    UsuarioRolEdicionComponent,
   
  ],
  entryComponents: [ DialogoComponent, DialogoDetalleComponent, DialogoSignosComponent, ModalEliminarComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PdfViewerModule
  ],
  providers: [ {
    provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }